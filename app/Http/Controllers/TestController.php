<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Serivces\DatabaseCreationService;
use App;
use DB;
use Config;
use Artisan;
use App\User;

class TestController extends Controller
{
    protected $databaseCreationService;
    public function __construct(DatabaseCreationService $databaseCreationService)
    {
        $this->databaseCreationService = $databaseCreationService;

    }
    public function Db($name){

        $prices = new DatabaseCreationService;
        $prices = $prices->createDB($name);
        return $prices;
    }

    public function exctuteTable($name){

        $prices = new DatabaseCreationService;
        $prices = $prices->exctuteTable($name);
        return $prices;
    }

    public function test($name){

        DB::disconnect('mysql');
        DB::select("create database ".$name);
        Config::set('database.connections.mysql.database', $name);
        DB::connection('mysql')->reconnect();

        Artisan::call("migrate");
        DB::table('users')->insert([
            'name' => $name."sdfds",
            'email' => $name.'@email.com',
            'password' => bcrypt('password'),
        ]);
        DB::table('users')->insert([
            'name' => $name,
            'email' => $name.'ergerg@email.com',
            'password' => bcrypt('password'),
        ]);

        return User::all();
    }
}
